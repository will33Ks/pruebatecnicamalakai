﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Net;

namespace App_Weather
{  
    public partial class Form1 : Form
    {
        Conexiondb c = new Conexiondb();
        public Form1()
        {
            InitializeComponent();
        }

        string APIKey = "30ddf01c6e81b226beb81b2eb1030993";

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void TbCity_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            getWeather();
        }

        void getWeather()
        {
            using (WebClient web = new WebClient())
            {
                string url = string.Format("https://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}", TbCity.Text, APIKey);
                var json = web.DownloadString(url);
                Info.root Information = JsonConvert.DeserializeObject<Info.root>(json);

                pcIcon.ImageLocation = "https://api.openweathermap.org/img/w/" + Information.weather[0].icon + ".png";
                lbCondition.Text = Information.weather[0].main;
                lbDetails.Text = Information.weather[0].description;
                lbSunset.Text = convertDateTime(Information.sys.sunset).ToString();
                lbSunrise.Text = convertDateTime(Information.sys.sunrise).ToString();
              
                lbWindSpeed.Text = Information.wind.speed.ToString();
                lbPressure.Text = Information.main.pressure.ToString();

                c.insertar(TbCity.Text, lbCondition.Text, lbDetails.Text, lbSunset.Text, lbSunrise.Text, lbWindSpeed.Text, lbPressure.Text);
               
                DateTime convertDateTime(long millisec)
                {
                    DateTime day = new DateTime(2021, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc).ToLocalTime();
                    day = day.AddMilliseconds(millisec).ToLocalTime();
                     
                    return day;
                }



            }
        }

        
    }
}
