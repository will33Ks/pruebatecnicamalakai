﻿
namespace App_Weather
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.TbCity = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lbCondition = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbDetails = new System.Windows.Forms.Label();
            this.lbSunrise = new System.Windows.Forms.Label();
            this.lbSunset = new System.Windows.Forms.Label();
            this.lb = new System.Windows.Forms.Label();
            this.lbPressure = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbWindSpeed = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pcIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pcIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(221, 249);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 64);
            this.label1.TabIndex = 0;
            this.label1.Text = "City:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // TbCity
            // 
            this.TbCity.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TbCity.Location = new System.Drawing.Point(384, 241);
            this.TbCity.Name = "TbCity";
            this.TbCity.Size = new System.Drawing.Size(483, 72);
            this.TbCity.TabIndex = 1;
            this.TbCity.TextChanged += new System.EventHandler(this.TbCity_TextChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.Transparent;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.Color.White;
            this.btnSearch.Location = new System.Drawing.Point(972, 238);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(228, 75);
            this.btnSearch.TabIndex = 3;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lbCondition
            // 
            this.lbCondition.AutoSize = true;
            this.lbCondition.BackColor = System.Drawing.Color.Transparent;
            this.lbCondition.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCondition.ForeColor = System.Drawing.Color.White;
            this.lbCondition.Location = new System.Drawing.Point(195, 599);
            this.lbCondition.Name = "lbCondition";
            this.lbCondition.Size = new System.Drawing.Size(261, 64);
            this.lbCondition.TabIndex = 4;
            this.lbCondition.Text = "Condition";
            this.lbCondition.Click += new System.EventHandler(this.label2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(195, 876);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(232, 64);
            this.label2.TabIndex = 5;
            this.label2.Text = "Sunrise:";
            // 
            // lbDetails
            // 
            this.lbDetails.AutoSize = true;
            this.lbDetails.BackColor = System.Drawing.Color.Transparent;
            this.lbDetails.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDetails.ForeColor = System.Drawing.Color.White;
            this.lbDetails.Location = new System.Drawing.Point(195, 719);
            this.lbDetails.Name = "lbDetails";
            this.lbDetails.Size = new System.Drawing.Size(197, 64);
            this.lbDetails.TabIndex = 6;
            this.lbDetails.Text = "Details";
            this.lbDetails.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbSunrise
            // 
            this.lbSunrise.AutoSize = true;
            this.lbSunrise.BackColor = System.Drawing.Color.Transparent;
            this.lbSunrise.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSunrise.ForeColor = System.Drawing.Color.White;
            this.lbSunrise.Location = new System.Drawing.Point(444, 876);
            this.lbSunrise.Name = "lbSunrise";
            this.lbSunrise.Size = new System.Drawing.Size(121, 64);
            this.lbSunrise.TabIndex = 7;
            this.lbSunrise.Text = "N/A";
            this.lbSunrise.Click += new System.EventHandler(this.label4_Click);
            // 
            // lbSunset
            // 
            this.lbSunset.AutoSize = true;
            this.lbSunset.BackColor = System.Drawing.Color.Transparent;
            this.lbSunset.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSunset.ForeColor = System.Drawing.Color.White;
            this.lbSunset.Location = new System.Drawing.Point(444, 996);
            this.lbSunset.Name = "lbSunset";
            this.lbSunset.Size = new System.Drawing.Size(121, 64);
            this.lbSunset.TabIndex = 9;
            this.lbSunset.Text = "N/A";
            // 
            // lb
            // 
            this.lb.AutoSize = true;
            this.lb.BackColor = System.Drawing.Color.Transparent;
            this.lb.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb.ForeColor = System.Drawing.Color.White;
            this.lb.Location = new System.Drawing.Point(195, 996);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(218, 64);
            this.lb.TabIndex = 8;
            this.lb.Text = "Sunset:";
            // 
            // lbPressure
            // 
            this.lbPressure.AutoSize = true;
            this.lbPressure.BackColor = System.Drawing.Color.Transparent;
            this.lbPressure.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPressure.ForeColor = System.Drawing.Color.White;
            this.lbPressure.Location = new System.Drawing.Point(1483, 719);
            this.lbPressure.Name = "lbPressure";
            this.lbPressure.Size = new System.Drawing.Size(121, 64);
            this.lbPressure.TabIndex = 13;
            this.lbPressure.Text = "N/A";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(1085, 719);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(268, 64);
            this.label5.TabIndex = 12;
            this.label5.Text = "Pressure:";
            // 
            // lbWindSpeed
            // 
            this.lbWindSpeed.AutoSize = true;
            this.lbWindSpeed.BackColor = System.Drawing.Color.Transparent;
            this.lbWindSpeed.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWindSpeed.ForeColor = System.Drawing.Color.White;
            this.lbWindSpeed.Location = new System.Drawing.Point(1483, 599);
            this.lbWindSpeed.Name = "lbWindSpeed";
            this.lbWindSpeed.Size = new System.Drawing.Size(121, 64);
            this.lbWindSpeed.TabIndex = 11;
            this.lbWindSpeed.Text = "N/A";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(1085, 599);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(347, 64);
            this.label7.TabIndex = 10;
            this.label7.Text = "Wind Speed:";
            // 
            // pcIcon
            // 
            this.pcIcon.BackColor = System.Drawing.Color.Transparent;
            this.pcIcon.Location = new System.Drawing.Point(204, 386);
            this.pcIcon.Name = "pcIcon";
            this.pcIcon.Size = new System.Drawing.Size(252, 160);
            this.pcIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcIcon.TabIndex = 14;
            this.pcIcon.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(19F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(2662, 1667);
            this.Controls.Add(this.pcIcon);
            this.Controls.Add(this.lbPressure);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbWindSpeed);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbSunset);
            this.Controls.Add(this.lb);
            this.Controls.Add(this.lbSunrise);
            this.Controls.Add(this.lbDetails);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbCondition);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.TbCity);
            this.Controls.Add(this.label1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pcIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TbCity;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lbCondition;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbDetails;
        private System.Windows.Forms.Label lbSunrise;
        private System.Windows.Forms.Label lbSunset;
        private System.Windows.Forms.Label lb;
        private System.Windows.Forms.Label lbPressure;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbWindSpeed;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pcIcon;
    }
}

